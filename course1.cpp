#include <iostream>

// GLEW
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>

/*
// Shader loading class
#include "Shader.h"
*/

// Prototypes for callback functions
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);

// Dimension of the window
const GLuint WIDTH = 800, HEIGHT = 600;

// Main function, it is here that we initialize what is relative to the context
// of OpenGL and that we launch the main loop
int main()
{
    std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
    // Initialization of GLFW
    glfwInit();

    // Specify the OpenGL version to be used (3.3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //.3
    // Disable the deprecated functions
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // Necesary for Mac OS 
    #ifdef __APPLE__
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif
    // Prevent the change of dimension of the window
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    
    // Create the application window
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "MyWindow", nullptr, nullptr);    
    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    
    // Associate the created window with the OpenGL context
    glfwMakeContextCurrent(window);
    // Associate the callback with the pressure of a key on the keyboard
    glfwSetKeyCallback(window, key_callback);

    // Variable global that allows to ask the GLEW library to find the modern 
    // functions of OpenGL
    glewExperimental = GL_TRUE;
    // Initializing GLEW to retrieve pointers from OpenGL functions
    if (glewInit() != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }

    // Transform the window dimensions to the dimensions of OpenGL (between -1 and 1)
    int width, height;
    // Recover the dimensions of the window created above
    glfwGetFramebufferSize(window, &width, &height); 
    glViewport(0, 0, width, height);

    /*
    // Build and compile our vertex and fragment shaders
    Shader shaders("shaders/c1/default.vertexshader", 
        "shaders/c1/default.fragmentshader");

    // Declare vertices to draw our triangle
    GLfloat vertices[] = {
        0.5f, -0.5f, 0.0f, // Bottom right point
       -0.5f, -0.5f, 0.0f, // Bottom left point
        0.0f,  0.5f, 0.0f  // Top point
    };
    
    // Declare the identifiers of our VBO and VAO
    GLuint VBO, VAO;
    // Inform OpenGL to generate one VAO
    glGenVertexArrays(1, &VAO);
    // Inform OpenGL to generate one VBO
    glGenBuffers(1, &VBO);
    
    // Bind the VAO as a common object to be used for the following
    // operations in the context of OpenGL
    glBindVertexArray(VAO);
    // Bind the VBO to the current buffer to be used for the following
    // operations in the context of OpenGL
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    // Associate the vertices stored in "vertices" to our buffer
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // Associate the current VAO with a new attribute with number 0 
    // (whose values are stored in the current buffer)
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
    // Allow the use of the attribute numbered 0 in our vertex shader
    glEnableVertexAttribArray(0);
    
    // VBO is detached from the current buffer in the OpenGL context
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // VAO is detached from the current object in the OpenGL context
    glBindVertexArray(0);
    */

    // The main loop of the program
    while (!glfwWindowShouldClose(window))
    {
        // Retrieve events and call the corresponding callback functions
        glfwPollEvents();

        // Replace the background color buffer of the window
        glClearColor(0.f, 0.59f, 0.54f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        /*
        // Inform OpenGL that we want to use the shaders we have created
        shaders.Use();

        // Bind the VAO as a current object in the context of OpenGL
        glBindVertexArray(VAO);
        // Draw the current object
        glDrawArrays(GL_TRIANGLES, 0, 3);
        // VBA is detached from the current object in the OpenGL context
        glBindVertexArray(0);
        */

        // Exchange rendering buffers to update the window with new content
        glfwSwapBuffers(window);
    }

    /*
    // Delete the objects and buffer that we created earlier
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    */
    
    // End of the program, we clean up the context created by the GLFW
    glfwTerminate();
    return 0;
}

// Callback function which is called when a key is pressed
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    std::cout << key << std::endl;
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    	// If the pressed key is "ESCAPE", the GLFW is asked to close the window
        glfwSetWindowShouldClose(window, GL_TRUE);
}
