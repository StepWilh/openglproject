#include <iostream>
#include <string>
#include <cmath>
#include <random>
#include <ctime>
#include <vector>
#include <fstream>
#include <typeinfo>
#include <sstream>
#include <iterator>
#include <numeric>
#include <algorithm>
#include <cstdlib>
#include <stdlib.h>
#include <time.h>
#include <math.h>

// GLEW
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Shader loading class
#include "Shader.h"
// Camera loading class
#include "Camera.h"
// Model loading class
#include "Model.h"

// Other Libs
#include <SOIL.h>

// Dimension of the window
const GLuint WIDTH = 800, HEIGHT = 600;

// Functions added----------------------------------------------------------------------------------------------
void readVectorData(vector<float> vec){
    for(int i=0; i<vec.size(); i++){
        cout<<"---"<<vec[i]<<endl;
    }
}
void readVectorDataMinMax(vector<float> vec){
    cout<<"x_min = "<<vec[0]<<endl;
    cout<<"z_min = "<<vec[1]<<endl;
    cout<<"x_max = "<<vec[2]<<endl;
    cout<<"z_max = "<<vec[3]<<endl;
}
void readVectorOfVectorData(vector<vector<float>> vec2vec){
    for(int i=0; i<vec2vec.size(); i++){
        cout<<"vector["<<i<<"] : "<<endl;
        for(int j=0; i<vec2vec[i].size(); j++){
            cout<<vec2vec[i][j]<<endl;
        }
        cout<<" "<<endl;
    }
}
float RandomFloat(float min, float max) {
    /*
     * @param min The minimum number that can retrn the function
     * @param max The maximum number that can retrn the function
     * @return a number between those limits
    **/
    srand(clock());
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = max - min;
    float r = random * diff;
    return min + r;
}

// Set a bounding box on object
vector<float> setBoundingBoxObject(string const path_obj){
    vector<float> bbox;
    float x_min, x_max, y_min, y_max, z_min, z_max;
    x_max = -10000000.0;
    y_max = -10000000.0;
    z_max = -10000000.0;
    x_min = 10000000.0;
    y_min = 10000000.0;
    z_min = 10000000.0;

    string word;
    std::ifstream file(path_obj);
    if (file.is_open()) {
        std::string line;
        while (getline(file, line)) {
            string line_string = (string)line;
            string first_char = line_string.substr(0,2);
            if (first_char=="v "){
                std::istringstream iss(line_string);
                std::vector<std::string> values(std::istream_iterator<std::string>{iss},std::istream_iterator<std::string>());
                float x = std::stof(values[1]);
                float y = std::stof(values[2]);
                float z = std::stof(values[3]);

                if (x > x_max){
                    x_max = x;
                }
                if (y > y_max){
                    y_max = y;
                }
                if(z > z_max){
                    z_max = z;
                }
                if (x < x_min){
                    x_min = x;
                }
                if (y < y_min){
                    y_min = y;
                }   
                if( z < z_min){
                    z_min = z;
                }
            }
        }
        file.close();
    }
    cout<<"/////////"<<endl;
    cout<<"x_max : "<<x_max<<endl;
    cout<<"y_max : "<<y_max<<endl;
    cout<<"z_max : "<<z_max<<endl;
    cout<<"x_min : "<<x_min<<endl;
    cout<<"y_min : "<<y_min<<endl;
    cout<<"z_min : "<<z_min<<endl;
    
    bbox.push_back(x_min);
    bbox.push_back(z_min);
    bbox.push_back(x_max);
    bbox.push_back(z_max);

    return bbox;
}
// vector to resize a boungind box object from scale
vector<float> resizeBoundingBoxObjectFromScale(vector<float> bbox, float rescale){
    for (int i = 0; i<bbox.size(); i++){
        bbox[i] = bbox[i]*rescale;
    }
    return bbox;
}

// function about collision detection
bool detectCollision(vector<float> coords_area1, vector<float> coords_area2){
    cout<<"*****detectCollision*****"<<endl;

    
    /*
    if (coords_area1[2]<coords_area2[0]){
        cout<<"c1"<<endl;
        //return true;
    }
    if(coords_area1[0]>coords_area2[2]){
        cout<<"c2"<<endl;
        ///return true;
    }
    if(coords_area1[3]<coords_area2[1]){
        cout<<"c3"<<endl;
        //return true;
    }
    if(coords_area1[1]>coords_area2[3]){
        cout<<"c4"<<endl;
        //return true;
    }
    */
    
    
    if (coords_area1[0]>coords_area2[2] || coords_area1[2]<coords_area2[0] || coords_area1[1]>coords_area2[3] || coords_area1[3]<coords_area2[1]){
        cout<<" -- no collision"<<endl;
        return false;
    } else {
        cout<<" ++ collision"<<endl;
        return true;
    }
    
}

bool collision(vector<vector<float>> list_bbox_obj2draw, vector<float> bbox_obj2check){
    cout<<"size list_bbox_obj2draw : "<<list_bbox_obj2draw.size()<<endl;
    for (int i=0; i<list_bbox_obj2draw.size(); i++){
        if(detectCollision(list_bbox_obj2draw[i], bbox_obj2check) == true){
            cout<<"COLLISION IDENTIFIEE"<<endl;
            return true;
        }
    }
    cout<<"Aucune collision avec les objets déjà sets"<<endl;
    return false;
}

// set dimensions from bounding box
vector<float> dimensionsFromBBox(vector<float> bbox){
    float dim_x = bbox[0]- bbox[1];
    float dim_y = bbox[2]- bbox[3];
    vector<float> dim;
    dim.push_back(dim_x);
    dim.push_back(dim_y);
    return dim;
}

// vector of bounding box world
vector<float> pointToBoundingBoxWorld(vector<float> point, vector<float> bbox){
    float x_point = point[0];
    float y_point = point[1];

    float x_min_obj = bbox[0];
    float y_min_obj = bbox[1];
    float x_max_obj = bbox[2];
    float y_max_obj = bbox[3];

    float x_min_world = x_point + x_min_obj;
    float y_min_world = y_point + y_min_obj;
    float x_max_world = x_point + x_max_obj;
    float y_max_world = y_point + y_max_obj;

    vector<float> bbox_world;
    // return left right bottom top
    bbox_world.push_back(x_min_world);
    bbox_world.push_back(y_min_world);
    bbox_world.push_back(x_max_world);
    bbox_world.push_back(y_max_world);

    return bbox_world;
}



glm::mat4 drawObject(Model m, glm::mat4 mod, GLint ml, Shader s, vector<float> translate_val, float rotate_value, vector<int> rotate_values, float scale){
    /*
     * The method to draw an object 
     * @param m The model to draw
     * @param The shader to use for drawing
     * @param translate_val The values of translation to make
             translate_val[0] : translation on x axis
             translate_val[1] : translation on y axis
             translate_val[2] : translation on z axis
     * @param rotate_val The values of rotation to make
             rotate_val[0] : rotation on x axis
             rotate_val[1] : rotation on y axis
             rotate_val[2] : rotation on z axis
     * @param scale The value of scale
    **/
      
    mod = glm::translate(mod, glm::vec3(translate_val[0], translate_val[1], translate_val[2]));
    mod = glm::rotate(mod, rotate_value, glm::vec3(rotate_values[0], rotate_values[1], rotate_values[2]));    
    mod = glm::scale(mod, glm::vec3(scale, scale, scale));
    glUniformMatrix4fv(ml, 1, GL_FALSE, glm::value_ptr(mod));   
    m.Draw(s);    
    mod = glm::scale(mod, glm::vec3(1/scale, 1/scale, 1/scale));
    mod = glm::rotate(mod, -rotate_value, glm::vec3(rotate_values[0], rotate_values[1], rotate_values[2]));
    mod = glm::translate(mod, glm::vec3(-translate_val[0], -translate_val[1], -translate_val[2]));
    glUniformMatrix4fv(ml, 1, GL_FALSE, glm::value_ptr(mod));   
    return mod;   
}


//---------------------------------------------------------------------------------------------------------------

// Define pi
const float pi = atan(1)*4;

// Car speed for the cars moving on the road
GLfloat car_speed(6.0f);



// Main function, it is here that we initialize what is relative to the context
// of OpenGL and that we launch the main loop
int main()
{
    srand(clock());
    std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
    // Initialization of GLFW
    glfwInit();

    // Specify the OpenGL version to be used (3.3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //.3
    // Disable the deprecated functions
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // Necesary for Mac OS 
    #ifdef __APPLE__
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif
    // Prevent the change of dimension of the window
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    
    // Create the application window
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "MyWindow", nullptr, nullptr);    
    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    
    // Associate the created window with the OpenGL context
    glfwMakeContextCurrent(window);
    // Associate the callback with the pressure of a key on the keyboard
    glfwSetKeyCallback(window, key_callback);

    // Variable global that allows to ask the GLEW library to find the modern 
    // functions of OpenGL
    glewExperimental = GL_TRUE;
    // Initializing GLEW to retrieve pointers from OpenGL functions
    if (glewInit() != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }    

    // Transform the window dimensions to the dimensions of OpenGL (between -1 and 1)
    int width, height;
    // Recover the dimensions of the window created above
    glfwGetFramebufferSize(window, &width, &height); 
    glViewport(0, 0, width, height);

    // Allow to test the depth information
    glEnable(GL_DEPTH_TEST);
    
    // Build and compile our vertex and fragment shaders
    Shader shaders("shaders/c3/default.vertexshader", 
        "shaders/c3/default.fragmentshader");
    
    // Setting values of the area to draw -----------------------------------------------------------------------------------------------------------------
    float top_x = 50.0f;
    float top_y = 50.0f;
    float bottom_x = -50.0f;
    float bottom_y = -50.0f;
    
    // Declare the positions and uv coordinates in the same buffer
    GLfloat vertices[] = {
        // Positions       // Colors         // UV
         top_x,  0.0f, top_y, 1.0f, 0.0f, 0.0f, 30.0f, 30.0f, // Top right point
         top_x, 0.0f, bottom_y, 0.0f, 1.0f, 0.0f, 30.0f, 0.0f, // Bottom right point
         bottom_x, 0.0f, bottom_y, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, // Bottom left point
         bottom_x,  0.0f, top_y, 1.0f, 1.0f, 0.0f, 0.0f, 30.0f, // Top left point
    };

    // The table of indices to rebuild our triangles
    GLushort indices[] = {  
        0, 1, 3,   // First triangle
        1, 2, 3    // Second triangle
    }; 
    
    // Declare the identifiers of our VAO, VBO and EBO
    GLuint VAO, VBO, EBO;
    // Inform OpenGL to generate one VAO
    glGenVertexArrays(1, &VAO);
    // Inform OpenGL to generate one VBO
    glGenBuffers(1, &VBO);
    // Inform OpenGL to generate one EBO
    glGenBuffers(1, &EBO); 
    
    // Bind the VAO
    glBindVertexArray(VAO);
    // Bind and fill the VBO
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // Bind and fill the EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // Color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), 
        (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
    // UV coordinates
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), 
        (GLvoid*)(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);
    
    // VBO is detached from the current buffer in the OpenGL context
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // VAO is detached from the current object in the OpenGL context
    glBindVertexArray(0);
    // EBO is detached (the last one!)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); 

    // Declare the texture identifier
    GLuint texture; 
    // Generate the texture
    glGenTextures(1, &texture); 
    // Bind the texture created in the global context of OpenGL
    glBindTexture(GL_TEXTURE_2D, texture); 
    // Method of wrapping
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); 
    // Filtering method
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    // Loading the image file using the SOIL lib
    int twidth, theight;
    unsigned char* data = SOIL_load_image("texture/grass.png", 
        &twidth, &theight, 0, SOIL_LOAD_RGB);
    // Associate the image data with texture
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, twidth, theight, 
        0, GL_RGB, GL_UNSIGNED_BYTE, data);
    // Generate the mipmap
    glGenerateMipmap(GL_TEXTURE_2D);
    // Free the memory
    SOIL_free_image_data(data);
    // Unbind the texture
    glBindTexture(GL_TEXTURE_2D, 0);

    // Camera
    Camera camera(glm::vec3(0.0f, 0.0f, 5.0f), window);

    //Model object("model/nanosuit/nanosuit.obj");
    Model house("model/house/farmhouse_obj.obj");
    Model tree("model/tree/Tree.obj");
    Model lamp("model/lamp/streetlamp_correct.obj");
    Model lampLight("model/lamp/streetlamplight.obj");
    Model roadSectionX("model/road_copie/RoadSectionX/10562_RoadSectionCross_v2-L3_MODIF.obj");
    Model roadSectionStraight("model/road_copie/RoadSectionStraight/10563_RoadSectionStraight_v1-L3_modif2.obj");
    Model roadSectionT("model/road_copie/RoadSectionTurn/10564_RoadSectionT_v2-L3_MODIF.obj");
    Model roadSectionTurn("model/road_copie/RoadSectionTurn/untitled.obj");
    Model car("model/car/car.obj");

    vector<float> bbox_tree = setBoundingBoxObject("model/tree/Tree.obj");
    vector<float> bbox_house = setBoundingBoxObject("model/house/farmhouse_obj.obj");
    
    
    //Initialisation de dates et d'intervalles de temps pour le déplacement de la voiture
    GLfloat deltaT(0);
    GLfloat lastT;
    lastT = glfwGetTime();

    // Define number of cars
    GLint nbVoitures(20);

    glm::vec3 carPos[nbVoitures];
    glm::vec3 carRot[nbVoitures];

    float traffic_dir_0 = 0.45f; // z of the position axis
    float traffic_dir_1 = -traffic_dir_0;
    
    // Setting the initial position and rotation for every car
    for(int i(0); i<nbVoitures; i++){
        float traffic_dir = (float)(rand() % 2); // Trafic direction is set to 0 or 1;
        carRot[i] = glm::vec3(0.0f, traffic_dir, 0.0f); // Rotation with an angle of pi
        
        // Random position of the car in the road area, and inside one of the 2 Trafic directions
        if (traffic_dir == 0.0){
            carPos[i] = glm::vec3(RandomFloat(-50.0, 50.0), 0.0f, traffic_dir_0);
        } else {
            carPos[i] = glm::vec3(RandomFloat(-50.0, 50.0), 0.0f, traffic_dir_1);
        }
    }
    

    // Bounding boxes of the objects drawn
    //vector<float> bbox_house = setBoundingBoxObject("/home/antoine/ProjetOpenGL/antoine/opengl/model/house/farmhouse_obj.obj");

    /* ----------------------------------------------------------------------------------------------------------------------------*/
    // ***************************************************** PROPERTIES objects *****************************************************
    /*
        We will draw some type of objectifs in the area : 
            - lamps
            - houses
            - trees

        First, we set the number of each to draw, and the rescale value to give them
    */

    // Number of houses and scales
    int nb_houses = rand() % 10 + 1;
    nb_houses = 6;
    cout<<"nb_houses : "<<nb_houses<<endl;
    float scale_house = 0.3f;

    // Number of lamps and scales
    int nb_lamp = rand() % 20 + 1;
    cout<<"nb_lamp : "<<nb_lamp<<endl;
    //float scale_lamp = 0.3f;

    // Number of trees and scales
    int nb_trees = rand() % 20 + 1;
    cout<<"nb_trees : "<<nb_trees<<endl;
    float scale_trees = 1.0f;



    /*
        Then we set the coordinates of each object to draw
        In order to don't draw over object already drawn, we get the coordinates of each element drawn by going through the .obj file for each type of object
    */    
    cout<<"----"<<endl;
    readVectorDataMinMax(bbox_house);
    cout<<"----"<<endl;

    vector<vector<float>> coords_objects;
    vector<vector<float>> bbox_coords_objects;

    vector<vector<float>> coords_world_houses;
    vector<vector<float>> bbox_world_houses;
    
    for (int i=0 ; i < nb_houses;i++){
        int nb_col = 1; 
        while(nb_col < 10){

            // Generate random coordinates
            float tranlate_x = RandomFloat(bottom_x, top_x);
            float tranlate_y = RandomFloat(bottom_y, top_y);
            
            //cout<<"random floats : "<<endl;
            vector<float> coords_house;
            coords_house.push_back(tranlate_x);
            coords_house.push_back(tranlate_y);
            
            vector<float> coords_bbox_house = pointToBoundingBoxWorld(coords_house, bbox_house);
            //cout<<"size coords_bbox_house : "<<coords_bbox_house.size()<<endl;
            //cout<<coords_world_houses.size()<<endl;
            if(coords_world_houses.size()<1){
                //cout<<"if"<<endl;
                // Adding bounding box to list :
                bbox_world_houses.push_back(coords_bbox_house);
                // Adding coords world houses : 
                coords_world_houses.push_back(coords_house);
                break;
            } 
            else {  
                cout<<"else"<<endl;
                // if there's already 1 or several object to draw : 
                if (collision(bbox_world_houses, coords_bbox_house) == false){
                    // if there's no collision with the first objects to draw :
                    // Adding bounding box to list :
                    bbox_world_houses.push_back(coords_bbox_house);
                    // Adding coords world houses : 
                    coords_world_houses.push_back(coords_house);
                    break;
                }
            }
            nb_col +=1;         
        }
        cout<<"coords_world_houses : "<<coords_world_houses.size()<<endl;
        cout<<"bbox_world_houses : "<<bbox_world_houses.size()<<endl;
        cout<<"Nombre de maisons actuel : "<<i+1<<endl;
    }

    int c = 0;
    // The main loop of the program
    while (!glfwWindowShouldClose(window))
    {
        // Retrieve events and call the corresponding callback functions
        glfwPollEvents();
        camera.Do_Movement();
        camera.Switch_Mode();
        std::cout << "Camera Pos X : " << camera.Position.x << std::endl;
        std::cout << "Camera Pos Y : " << camera.Position.y << std::endl;
        std::cout << "Camera Pos Z : " << camera.Position.z << std::endl;
        std:: cout << "LEVEL : "<< camera.level << std::endl;

        // Replace the background color buffer of the window
        GLfloat timeValue = glfwGetTime();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // Inform OpenGL that we want to use the shaders we have created
        shaders.Use();

        // Recover the identifiers of the global variables of the shader
        GLint modelLoc = glGetUniformLocation(shaders.Program, "model");
        GLint viewLoc  = glGetUniformLocation(shaders.Program, "view");
        GLint projLoc  = glGetUniformLocation(shaders.Program, "projection");

        // Model matrix (translation, rotation and scale)
        glm::mat4 model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f));
        // Camera matrix
        glm::mat4 view;
        view = camera.GetViewMatrix();
        // Projection matrix (generate a perspective projection matrix)
        glm::mat4 projection;
        projection = glm::perspective(45.0f, (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 100.0f);


        // Update the global variables of the shader
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

        // Bind the VAO as a current object in the context of OpenGL
        glBindVertexArray(VAO);
        // Activate texture 0
        glActiveTexture(GL_TEXTURE0);
        // Bind the texture
        glBindTexture(GL_TEXTURE_2D, texture);
        // Associate the texture with the shader
        glUniform1i(glGetUniformLocation(shaders.Program, "modelTexture"), 0);
        // Draw the current object
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
        // VBA is detached from the current object in the OpenGL context
        glBindVertexArray(0);

        // ROAD -------------------------------------------------------------------

        // Road parameters
        float start_road_x = -50.0;
        float end_road_x = 50.0;
        
        float step_x = 0.43575 * 2; // Step in x for drawing between the objects
        float cur_x = start_road_x;
        float tot_x = start_road_x;

        float y_lamp_road = 1.3f; // Distance between center of the road and lamp
        
        // Drawing road
        
        cur_x = start_road_x;
        while (cur_x<end_road_x){
            // Draw road
            model = glm::translate(model, glm::vec3(cur_x, 0.0f, 0.0f));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            roadSectionStraight.Draw(shaders);
            
            // Draw lamps next to the road
            model = glm::translate(model, glm::vec3(0.0f, 0.0f, y_lamp_road));
            model = glm::scale(model, glm::vec3(0.1));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            lamp.Draw(shaders);
            model = glm::scale(model, glm::vec3(1/0.1));
            model = glm::translate(model, glm::vec3(0.0f, 0.0f, -y_lamp_road*2));
            model = glm::scale(model, glm::vec3(0.1));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            lamp.Draw(shaders);
            model = glm::scale(model, glm::vec3(1/0.1));
            model = glm::translate(model, glm::vec3(0.0f, 0.0f, y_lamp_road));
            model = glm::translate(model, glm::vec3(-cur_x, 0.0f, 0.0f));
            cur_x += step_x;
        }

  // HOUSES
        //cout<<"--------------------------------------------------------"<<endl;
        float scale_houses = 0.1;
        for (int i = 0; i < coords_world_houses.size(); i++) {
            cout<<"tr_x : "<<coords_world_houses[i][0]<<endl;
            cout<<"tr_y : "<<coords_world_houses[i][1]<<endl;

            model = glm::translate(model, glm::vec3(coords_world_houses[i][0], 0.0f, coords_world_houses[i][1]));
            model = glm::scale(model, glm::vec3(scale_houses, scale_houses, scale_houses));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            house.Draw(shaders);
            model = glm::scale(model, glm::vec3(1/scale_houses, 1/scale_houses, 1/scale_houses));
            model = glm::translate(model, glm::vec3(-coords_world_houses[i][0], 0.0f, -coords_world_houses[i][1]));
            break;
        } 

        // CARS --------------------------------------------------------------------

        // Updating variables time
        deltaT = glfwGetTime() - lastT;
        lastT = glfwGetTime();

        // Loop to update the position of the positions of all the cars
        for(int current_car(0); current_car < nbVoitures; current_car++){      
            model = glm::mat4(1.0f);
            
            if(carRot[current_car].y == 0.0f){
                // If trafic direction is 0.0, decrement position of the car
                carPos[current_car].x  -=  (deltaT * car_speed);
                if(carPos[current_car].x < start_road_x){
                    // If car is out of the road dimensions, we rotate the car and switch trafic lane
                    carRot[current_car].y = 1.0f;
                    carPos[current_car].x = -49.9f;
                    carPos[current_car].z -= 0.90f;
                }
            }   
            if(carRot[current_car].y == 1.0f){
                // If trafic direction is 0.0, increment position of the car
                carPos[current_car].x  +=  (deltaT * car_speed);
                if(carPos[current_car].x > end_road_x){
                    // If car is out of the road dimensions, we rotate the car and switch trafic lane
                    carRot[current_car].y = 0.0;
                    carPos[current_car].x = 49.9f;
                    carPos[current_car].z += 0.90f;
                }
            }

            if(carRot[current_car].y == 0.0f){    
                // If trafic direction is 0.0, only translate the car
                model = glm::translate(model, glm::vec3(carPos[current_car].x, carPos[current_car].y+0.35f, carPos[current_car].z));
            } else {
                // If trafic direction is 1.0, translate the car and rotate it to switch trafic direction
                model = glm::translate(model, glm::vec3(carPos[current_car].x, carPos[current_car].y+0.35f, carPos[current_car].z));
                model = glm::rotate(model, pi, glm::vec3(0, 1, 0));
            }
            
            // Draw a rescaled car
            //model = glm::scale(model, glm::vec3(0.1f));
            glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
            car.Draw(shaders);
            //model = glm::scale(model, glm::vec3(1/0.008));
            model = glm::translate(model, glm::vec3(-carPos[current_car].x, carPos[current_car].y, carPos[current_car].z));     
        }

        // TREE DRAWING 
        // We would have generate some trees like we made it with the houses:
        // * Get bounding box on a obj file "tree.obj"
        // * Generate a random coordinate where to translate the tree
        // * Test if an object is already drawn there (including bounding box)
        model = glm::mat4(1.0f);
        model = glm::scale(model, glm::vec3(1.0f, 1.0f, 1.0f));
        model = glm::translate(model, glm::vec3(-15.0f, 0.0f, 5.0f));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        tree.Draw(shaders);

        // LIGHT AND SKY COLOR --------------------------------------------------------------------

        //limit positions when the dusk and dawn begin or end
        GLfloat DUSK_ANGLE(3*M_PI/5);
        GLfloat DAWN_ANGLE(4*M_PI/3);

        GLint DAY_DURATION(60);

        // sun light position for light
        GLint lightPos = glGetUniformLocation(shaders.Program, "lightPos");
        GLint lightColor = glGetUniformLocation(shaders.Program, "lightColor");
        GLint ambientStrength = glGetUniformLocation(shaders.Program, "ambientStrength");
        GLint specularStrength = glGetUniformLocation(shaders.Program, "specularStrength");

        //abient strength and specular
        GLfloat ambStr(0.05f);
        GLfloat specStr(0.3f);
        //On la positionne en relatif par rapport à la caméra, afin qu'elle "suive" l'utilisateur
        glm::vec4 lPos(camera.Position.x, camera.Position.y + 300.0f, camera.Position.z, 1.0f);
        glm::vec3 lColor(1.0f, 1.0f, 1.0f);
        //light rotation around z axe
        glm::mat4 rot;
        GLfloat angle = glm::mod(2*M_PI*(glfwGetTime()/DAY_DURATION), 2*M_PI);
        rot = glm::rotate(rot, angle, glm::vec3(0.0f, 0.0f, 1.0f));
        lPos = rot * lPos;
        glm::vec3 clearColor;
        //horizon simulation, the light is low when the light is below the horizon 
        //very low light with dark blue
        if (angle > DUSK_ANGLE && angle < DAWN_ANGLE){
            lColor = glm::vec3(0.0f, 0.0f, 0.05f);

            //night sky color
            clearColor = glm::vec3(0.05f, 0.04f, 0.1f);

            //very low atmosphere light
            ambStr = 0.05f;
        
        //Twilight time, light goes to orange then to dark blue
        } else if (angle > M_PI/4 && angle < DUSK_ANGLE){
            // x is 1 at the begining of the twilight time, 0 at the end
            double x = (DUSK_ANGLE - angle)/(DUSK_ANGLE - M_PI/4);

            //'orange' twilight color
            lColor = glm::vec3(x * 1.0f, pow(x, 2) * 1.0f, pow(x, 4) * 1.0f + 0.05f);

            //'Sky' color, x is used for R calculation as R(0) = R(1) = 0 et R(0.5) = 0.5
            GLfloat r = (-1*pow(x,2) + 1.8*x) * 0.6f + 0.05;
            GLfloat g = pow(x, 2) * 0.8f + 0.04;
            GLfloat b = pow(x, 4) * 0.9f + 0.1;

            //clearColor = glm::vec3((-2*pow(x,2) + 2*x) * 0.9f, pow(x, 2) * 0.7f + 0.02, pow(x, 4) * 0.7f + 0.05f);
            clearColor = glm::vec3(r,g,b);
            //Lumière ambiante décroissante et continue, cohérente avec les valeurs de jour et de nuit
            ambStr = x * 0.65f + 0.05f;


        //Dawn light
        } else if (angle > DAWN_ANGLE && angle < 5*M_PI/3){
            //x is 0 at the begining, 1 at the end
            double x = (DAWN_ANGLE - angle)/(DAWN_ANGLE - 5*M_PI/3);

            //dawn sky color
            lColor = glm::vec3(pow(x,4) * 0.8f, pow(x, 2) * 1.0f, x * 1.0f + 0.05f);

            //'Sky' color,  x is used for R calculation as R(0) = R(1) = 0 et R(0.5) = 0.5
            GLfloat r = (-1*pow(x,2) + 1.9*x) * 0.5f + 0.05;
            GLfloat g = pow(x, 2) * 0.8f + 0.04;
            GLfloat b = pow(x, 4) * 0.82f + 0.1;

            clearColor = glm::vec3(r,g,b);
            
            //ambient light
            ambStr = x * 0.65f + 0.05f;

        //Day time
        } else{
            //white light color from the sun
            lColor = glm::vec3(1.0f, 1.0f, 1.0f);
            //Blue sky
            clearColor = glm::vec3(0.52f, 0.81f, 0.92f);
            //Strong ambient light
            ambStr = 0.7f;

        }

        


        //Sky is colored and global variables are updated
        glClearColor(clearColor.x, clearColor.y, clearColor.z, 1.0f);
        glUniform3f(lightPos, lPos.x, lPos.y, lPos.z);
        glUniform3f(lightColor, lColor.x, lColor.y , lColor.z);
        glUniform1f(ambientStrength, ambStr);
        glUniform1f(specularStrength, specStr);

        // LAMP AND LIGHT DRAWING DEPENDING OF THE DAY/NIGHT CYCLE (EXPERIMENTAL)
        // We would have generate some light sources from lamps :
        // * Detect the daytime using the getTime function and angle variables like for "DUSK" or "DAWN"
        // * Remplace the street lamp object with lamp with light on object, a light is also generated. This case is when it is being dark during the twilight
        // * During the night, the lamp emits a orange-Yellow light giving a orange color to the ground
        // * During the dawn, remplace the lamp light on with lamp light off and remove the light.
/* // part of the code that world be added
        lmpPos = glm::vec3(15.0f, 1.0f, -5.0f);

        if (angle > M_PI/3 && angle < 8*M_PI/5){
            
            lmpColor = glm::vec3(1.0f, 0.65f, 0.1f);

        }else {
            lmpColor = glm::vec3(0.0f, 0.0f, 0.0f);

        }

        glUniform3f(lampPos, lmpPos.x, lmpPos.y , lmpPos.z);
        glUniform3f(lampColor, lmpColor.x, lmpColor.y , lmpColor.z);
*/
        //Lamp light caracteristics
        GLint lampPos = glGetUniformLocation(shaders.Program, "lampPos");
        GLint lampColor = glGetUniformLocation(shaders.Program, "lampColor");

        glm::vec3 lmpColor;
        glm::vec3 lmpPos;
        

        // Exchange rendering buffers to update the window with new content
        glfwSwapBuffers(window);
    }
    

    // Delete the objects and buffer that we created earlier
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
    
    // End of the program, we clean up the context created by the GLFW
    glfwTerminate();
    return 0;
}
