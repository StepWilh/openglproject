#include <iostream>

// GLEW
#include <GL/glew.h>
// GLFW
#include <GLFW/glfw3.h>

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Shader loading class
#include "Shader.h"

// Prototypes for callback functions
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);

// Dimension of the window
const GLuint WIDTH = 800, HEIGHT = 600;

// Main function, it is here that we initialize what is relative to the context
// of OpenGL and that we launch the main loop
int main()
{
    std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
    // Initialization of GLFW
    glfwInit();

    // Specify the OpenGL version to be used (3.3)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //.3
    // Disable the deprecated functions
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // Necesary for Mac OS 
    #ifdef __APPLE__
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif
    // Prevent the change of dimension of the window
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    
    // Create the application window
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "MyWindow", nullptr, nullptr);    
    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    
    // Associate the created window with the OpenGL context
    glfwMakeContextCurrent(window);
    // Associate the callback with the pressure of a key on the keyboard
    glfwSetKeyCallback(window, key_callback);

    // Variable global that allows to ask the GLEW library to find the modern 
    // functions of OpenGL
    glewExperimental = GL_TRUE;
    // Initializing GLEW to retrieve pointers from OpenGL functions
    if (glewInit() != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }    

    // Transform the window dimensions to the dimensions of OpenGL (between -1 and 1)
    int width, height;
    // Recover the dimensions of the window created above
    glfwGetFramebufferSize(window, &width, &height); 
    glViewport(0, 0, width, height);
    
    // Build and compile our vertex and fragment shaders
    Shader shaders("shaders/c2/default.vertexshader", 
        "shaders/c2/default.fragmentshader");
    
    // Declare the colored cube
    GLfloat vertices[] = {
        // Positions          // Colors          // Normals
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f,  0.0f, /*0.0f,  0.0f, -1.0f,*/
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f,  0.0f, /*0.0f,  0.0f, -1.0f,*/
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f,  0.0f, /*0.0f,  0.0f, -1.0f,*/
        -0.5f,  0.5f, -0.5f,  1.0f, 0.0f,  0.0f, /*0.0f,  0.0f, -1.0f,*/

        -0.5f, -0.5f,  0.5f,  1.0f, 1.0f,  0.0f, /*0.0f,  0.0f,  1.0f,*/
         0.5f, -0.5f,  0.5f,  1.0f, 1.0f,  0.0f, /*0.0f,  0.0f,  1.0f,*/
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,  0.0f, /*0.0f,  0.0f,  1.0f,*/
        -0.5f,  0.5f,  0.5f,  1.0f, 1.0f,  0.0f, /*0.0f,  0.0f,  1.0f,*/

        -0.5f,  0.5f,  0.5f,  0.0f, 0.1f,  0.0f, /*-1.0f,  0.0f,  0.0f,*/
        -0.5f,  0.5f, -0.5f,  0.0f, 0.1f,  0.0f, /*-1.0f,  0.0f,  0.0f,*/
        -0.5f, -0.5f, -0.5f,  0.0f, 0.1f,  0.0f, /*-1.0f,  0.0f,  0.0f,*/
        -0.5f, -0.5f,  0.5f,  0.0f, 0.1f,  0.0f, /*-1.0f,  0.0f,  0.0f,*/

         0.5f,  0.5f,  0.5f,  0.0f, 0.1f,  1.0f, /*1.0f,  0.0f,  0.0f,*/
         0.5f,  0.5f, -0.5f,  0.0f, 0.1f,  1.0f, /*1.0f,  0.0f,  0.0f,*/
         0.5f, -0.5f, -0.5f,  0.0f, 0.1f,  1.0f, /*1.0f,  0.0f,  0.0f,*/
         0.5f, -0.5f,  0.5f,  0.0f, 0.1f,  1.0f, /*1.0f,  0.0f,  0.0f,*/

        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,  1.0f, /*0.0f, -1.0f,  0.0f,*/
         0.5f, -0.5f, -0.5f,  0.0f, 0.0f,  1.0f, /*0.0f, -1.0f,  0.0f,*/
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f,  1.0f, /*0.0f, -1.0f,  0.0f,*/
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,  1.0f, /*0.0f, -1.0f,  0.0f,*/

        -0.5f,  0.5f, -0.5f,  0.5f, 0.5f,  0.5f, /*0.0f,  1.0f,  0.0f,*/
         0.5f,  0.5f, -0.5f,  0.5f, 0.5f,  0.5f, /*0.0f,  1.0f,  0.0f,*/
         0.5f,  0.5f,  0.5f,  0.5f, 0.5f,  0.5f, /*0.0f,  1.0f,  0.0f,*/
        -0.5f,  0.5f,  0.5f,  0.5f, 0.5f,  0.5f, /*0.0f,  1.0f,  0.0f,*/
    };
    
    // The table of indices to rebuild our cube
    GLshort indices[]{
    	0, 1, 2,
    	2, 3, 0,

    	4, 5, 6,
    	6, 7, 4,

        8, 9, 10,
        10, 11, 8,

        12, 13, 14,
        14, 15, 12,

        16, 17, 18,
        18, 19, 16,

        20, 21, 22,
        22, 23, 20
    };    
    
    // Declare the identifiers of our VAO, VBO and EBO
    GLuint VAO, VBO, EBO;
    // Inform OpenGL to generate one VAO
    glGenVertexArrays(1, &VAO);
    // Inform OpenGL to generate one VBO
    glGenBuffers(1, &VBO);
    // Inform OpenGL to generate one EBO
    glGenBuffers(1, &EBO);
    
    // Bind the VAO
    glBindVertexArray(VAO);
    
    // Bind and fill the VBO
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    // Bind and fill the EBO
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    // Color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), 
        (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
    
    // VBO is detached from the current buffer in the OpenGL context
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // VAO is detached from the current object in the OpenGL context
    glBindVertexArray(0);
    // EBO is detached (the last one!)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); 

    // The main loop of the program
    while (!glfwWindowShouldClose(window))
    {       
        // Retrieve events and call the corresponding callback functions
        glfwPollEvents();

        // Replace the background color buffer of the window
        glClearColor(0.f, 0.59f, 0.54f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        
        // Inform OpenGL that we want to use the shaders we have created
        shaders.Use();
        /*
	    // Recover the identifiers of the global variables of the shader
        GLint modelLoc = glGetUniformLocation(shaders.Program, "model");
        GLint viewLoc  = glGetUniformLocation(shaders.Program, "view");
        GLint projLoc  = glGetUniformLocation(shaders.Program, "projection");

        // Model matrix (translation, rotation and scale)
        glm::mat4 model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f));
        model = rotate(model, glm::radians(-135.0f), glm::vec3(1.0, 1.0, 0.0));
        model = glm::scale(model, glm::vec3(0.7f));
        // Camera matrix
        glm::mat4 view;
        view = glm::lookAt(glm::vec3(0.0f, 0.0f, 3.0f),
            glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        // Projection matrix (generate a perspective projection matrix)
        glm::mat4 projection;
        projection = glm::perspective(45.0f, (GLfloat)WIDTH / (GLfloat)HEIGHT, 0.1f, 100.0f);

        // Update the global variables of the shader
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        */

        // Bind the VAO as a current object in the context of OpenGL
        glBindVertexArray(VAO);
        // Draw the current object
        glDrawElements(GL_TRIANGLES, 3*12, GL_UNSIGNED_SHORT, 0);
        // VBA is detached from the current object in the OpenGL context
        glBindVertexArray(0);

        // Exchange rendering buffers to update the window with new content
        glfwSwapBuffers(window);
    }
    
    // Delete the objects and buffer that we created earlier
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
    
    // End of the program, we clean up the context created by the GLFW
    glfwTerminate();
    return 0;
}

// Callback function which is called when a key is pressed
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    std::cout << key << std::endl;
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    	// If the pressed key is "ESCAPE", the GLFW is asked to close the window
        glfwSetWindowShouldClose(window, GL_TRUE);
}
