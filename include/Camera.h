#pragma once

// Std. Includes
#include <vector>

// GL Includes
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glu.h>

#include <GLFW/glfw3.h>

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);

// Defines several possible options for camera movement. 
// Used as abstraction to stay away from window-system specific input methods
enum Camera_Movement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};

// the four differents camera 'levels' or modes
enum Camera_Level {
    GROUND,
    SKY,
    ANIMATED,
    STANDART
};

// Default camera values
const GLfloat YAW        = -90.0f;
const GLfloat PITCH      =  0.0f;
//const GLfloat SPEED      =  9.0f;
const GLfloat SPEED      =  1.0f;
const GLfloat SENSITIVTY =  0.25f;

bool keys[1024];
GLfloat lastX = 400;
GLfloat lastY = 300;
GLfloat xoffset;
GLfloat yoffset;
bool firstMouse = true;
bool mouse_on = false;



// An abstract camera class that processes input and calculates the corresponding
//  Eular Angles, Vectors and Matrices for use in OpenGL
class Camera
{
public:
    // Camera Attributes
    glm::vec3 Position;
    glm::vec3 Front;
    glm::vec3 Up;
    glm::vec3 Right;
    glm::vec3 WorldUp;
    // Eular Angles
    GLfloat Yaw;
    GLfloat Pitch;
    // Camera options
    GLfloat MovementSpeed;
    GLfloat MouseSensitivity;

    GLfloat deltaTime;
    GLfloat lastFrame;

    Camera_Level level;

    GLfloat roundTime = 0;

    // Constructor with vectors
    Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f),  GLFWwindow* window = nullptr, glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), GLfloat yaw = YAW, GLfloat pitch = PITCH,  Camera_Level level = GROUND) : 
    Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVTY), deltaTime(0.0f), lastFrame(0.0f)
    {

        
        this->Position = position;
        this->WorldUp = up;
        this->Yaw = yaw;
        this->Pitch = pitch;
        this->updateCameraVectors();
        this->level = level;

        glfwSetKeyCallback(window, key_callback);
        glfwSetCursorPosCallback(window, mouse_callback);
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    }

    // Returns the view matrix calculated using Eular Angles and the LookAt Matrix
    glm::mat4 GetViewMatrix()
    {
        return glm::lookAt(this->Position, this->Position + this->Front, this->Up);
    }

    // Processes input received from any keyboard-like input system. Accepts input parameter
    // in the form of camera defined ENUM (to abstract it from windowing systems)
    void ProcessKeyboard(Camera_Movement direction, GLfloat deltaTime)
    
    {
        GLfloat velocity = this->MovementSpeed * deltaTime;
        if(this->level == SKY){
            if (direction == FORWARD)
                this->Position += glm::vec3(0.0f, 0.0f, -1.0f)  * velocity;
            if (direction == BACKWARD)
                this->Position -= glm::vec3(0.0f, 0.0f, -1.0f) * velocity;
            if (direction == LEFT)
                this->Position -= glm::vec3(1.0f, 0.0f, 0.0f) * velocity;
            if (direction == RIGHT)
                this->Position += glm::vec3(1.0f, 0.0f, 0.0f) * velocity;

        }
        else if(this->level == GROUND || this->level == STANDART) {

            if (direction == FORWARD)
                this->Position += this->Front * velocity;
            if (direction == BACKWARD)
                this->Position -= this->Front * velocity;
            if (direction == LEFT)
                this->Position -= this->Right * velocity;
            if (direction == RIGHT)
                this->Position += this->Right * velocity;
        }
    }

    // Processes input received from a mouse input system. Expects the offset value
    // in both the x and y direction.
    void ProcessMouseMovement(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true)
    {
        if(this->level == GROUND || this->level == STANDART){
            xoffset *= this->MouseSensitivity;
            yoffset *= this->MouseSensitivity;

            this->Yaw   += xoffset;
            this->Pitch += yoffset;

            // Make sure that when pitch is out of bounds, screen doesn't get flipped
            if (constrainPitch)
            {
                if (this->Pitch > 89.0f)
                    this->Pitch = 89.0f;
                if (this->Pitch < -89.0f)
                    this->Pitch = -89.0f;
            }

            // Update Front, Right and Up Vectors using the updated Eular angles
            this->updateCameraVectors();
        }
        
    }

    // Moves/alters the camera positions based on user input
    void Do_Movement()
    {
        GLfloat currentFrame = glfwGetTime();
        this->deltaTime = currentFrame - this->lastFrame;
        this->lastFrame = currentFrame;

        // Camera controls
        if(keys[87])
            this->ProcessKeyboard(FORWARD, this->deltaTime);
        if(keys[83])
            this->ProcessKeyboard(BACKWARD, this->deltaTime);
        if(keys[65])
            this->ProcessKeyboard(LEFT, this->deltaTime);
        if(keys[68])
            this->ProcessKeyboard(RIGHT, this->deltaTime);

        if(mouse_on)
        {
            this->ProcessMouseMovement(xoffset, yoffset);
            mouse_on = false;
        }
        
        // camera behavior depending on camera mod
        if(this->level == GROUND)
            this->Position.y = 0.3f;
        else if(this->level == ANIMATED){

            this->Position.x = 30*cos((currentFrame)/4);
            this->Position.z = 30*sin((currentFrame)/4);
            this->Position.y = 20.0f;
            std::cout << "current frame : " << currentFrame  << std::endl;
            std::cout << "round time : " << currentFrame - this->roundTime  << std::endl;
            std::cout << "Camera position X : " << this->Position.x << std::endl;
            std::cout << "Camera position Y : " << this->Position.y << std::endl;
            std::cout << "Camera position Z : " << this->Position.z << std::endl;

            // gives a pitch to see the ground from the air
            this->Pitch = -30.0f;

            this->Yaw = 180 + 14*(currentFrame-this->roundTime);

            // when the camera made has made a circle, it turn back to an initial position and yaw
            if(Position.x > 29.99 && Position.z <= 0.01 && Position.z >= -0.1){
                this->Position.x = 0;
                this->Position.z = 30;
                this->Yaw = 180;
                this->roundTime = currentFrame;
                //glfwSetTime(0);
            }

            //gluLookAt(Position.x,Position.y,Position.z,0.0f,0.0f,0.0f,1.0f,1.0f,1.0f);
            std::cout << "YAW : " << this->Yaw << std::endl;
            
            //glm::lookAt(this->Position, this->Position + this->Front, this->Up);
		
            this->updateCameraVectors();
        }



    }

// when the user press space, the camera mode changes
    void Switch_Mode(){
        if(keys[32]){
            switch (this->level)
            {
            // go to the next camera mode with associated parameters
            case GROUND :
                this->level  =  SKY;
                this->Position.y = 70.0f;
                this->Pitch = -89.9f;
                this->Yaw   =  YAW;
                this->updateCameraVectors();
                this->Up    =  glm::vec3(0.0f, 0.0f, -1.0f);
                this->MovementSpeed = 9.0f;
                break;
            case SKY :
                this->level  =  ANIMATED;
                this->Up    = glm::vec3(0.0f, 1.0f, 0.0f);
                this->Pitch = 0.0f;
                this->updateCameraVectors();
                break;
            case ANIMATED :
                this->level  =  STANDART;
                this->Up    = glm::vec3(0.0f, 1.0f, 0.0f);
                this->Yaw = 0.0f;
                this->Pitch = 0.0f;
                this->updateCameraVectors();    
                break;        
                /* code */
            case STANDART :
                this->level  = GROUND;
                this->Pitch = 0.0f;
                this->Position.y = 1.0f;
                this->updateCameraVectors();
                this->Up    = glm::vec3(0.0f, 1.86f, 0.0f);
                this->MovementSpeed = 1.0f;
                break;
            default:
                break;
            }
            // Avoit any problems when the key is pressed during many frames
            keys[32] = false;
        }
    }


private:
    // Calculates the front vector from the Camera's (updated) Eular Angles
    void updateCameraVectors()
    {
        // Calculate the new Front vector
        glm::vec3 front;
        front.x = cos(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
        front.y = sin(glm::radians(this->Pitch));
        front.z = sin(glm::radians(this->Yaw)) * cos(glm::radians(this->Pitch));
        this->Front = glm::normalize(front);
        // Also re-calculate the Right and Up vector
        // Normalize the vectors, because their length gets closer to 0
        // the more you look up or down which results in slower movement.
        this->Right = glm::normalize(glm::cross(this->Front, this->WorldUp));  
        this->Up    = glm::normalize(glm::cross(this->Right, this->Front));
    }
};



// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    std::cout << key << std::endl;
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    
    if(action == GLFW_PRESS && key != 32)
        keys[key] = true;
    else if(action == GLFW_RELEASE && key != 32)
        keys[key] = false;
    else if(action == GLFW_PRESS && key == 32)
        keys[key] = !keys[key];
}


void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if(firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }
    
    xoffset = xpos - lastX;
    yoffset = lastY - ypos;
    
    lastX = xpos;
    lastY = ypos;

    mouse_on=true;	
    
    //this->ProcessMouseMovement(xoffset, yoffset);
}
