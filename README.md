# 3D urbain environnement generation

## Statement

This project has purpose to auto-generate a 3D environnement with urbains features : house, a moving car, trees and streetlamps. There is also a road crossing the land with land around and cars moving on the road.
These objects are placed spatially on a plane ground. The project was build using c++ and OpenGL library.

The following modules are inplemented in the project:
* Random world generation
* Dynamic light
* Camera set
* Planned moves (car moving on a road)

## Opening an image
The application must be opened **using Linux as Operating System**. To open a windows with our 3D environnement:
1. Unzip the project somewhere in your repostory
2. Go into the folder where the file "cmake_install.cmake" is located (in the project root normally)
3. Open a terminal redirecting to this folder
4. Install the following modules: 
cmake, glfw, glew, glsl, glm

``` $ apt-get install yourModuleToInstall```

5. Run the following instruction in order:

``` $ cmake . ```

``` $ make ```

6. One executable generated, run the following command:

``` $ ./course3 ```

7. The window is now open. Enjoy! :)

## Features

### Random World Generation
We implemented many algorithms to randomly generate structures on the ground.
the cars are generated along the road. Houses are generated randomly in the ground without collision.
* there is a choosen random number too set the number of object to generate
* All created object have their coordinates stored in a vector. Each of them have a bounding box used to avoid collisions between objects when generated.
* Example: one house type object is generated in the gound, with his bounding box, when another is generated and collide with an other object's bounding box, the object is created elsewere untill there is no collision with another object.
* Cars 

### Dynamic light
A day light source is implemented in the project by severals features:
* a light source moving in the sky and changing throught a day/night cycle
* background color changing depending of the day time
* light and background color set to create dusk and dawn atmosphere
* fixed light on during the night time coming from auto generated lamps
Implemented but not working correctly: Lamp could be lit during the night with a yellow-orange color. We would like to generate a light from lamp during night time like on the picture in img folder
(foreseen: get a list of lamp coordinates and use it to set light coordinates associated to each lamp generated, some code in fragmentshader have variables foreseen to make lamp lights work, the actual light, ready to use, work not correctly)

### Camera set

* In the first mode, **GROUND**, set when the user launch the application, the camera can be moved across the ground, in a parallel plan, like someone walking on the ground.
* The second mode, **SKY**, a visualization mode is displayed as the camera moves in the air and keeps pointing to the ground like for an aerial view.
* The third mode, **ANIMATED**, is a view where the camera moves freely and independantly without user's control. it turns around pointing at the landscape like in a movie.
* In the fourth mode, **STANDART**, the camera moves where the user is pointing at. We can fly in the air and go where we want.

## Controls
The user is allowed to move the camera depending of the camera mode by pressing keys. The mentioned keys are used for an AZERTY keyboard.
* Change camera mode : **SPACE**

the camera mode are set in this order: GROUND, SKY, ANIMATED, STANDART. After the last one, the mode is set to GROUND.

In the **GROUND** and **STANDART** mode:
* see around : **mouse moving**
* move forward : **z**
* move back : **S**
* move left : **Q**
* move right : **D**

In the **SKY** mode:
* move up : **Z**
* move down : **S**
* move left : **Q**
* move right : **D**

## authors

Stéphane Wilhelm
Antoine Laurendon
ENSG - TSI